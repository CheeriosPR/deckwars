﻿using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour {

	private GameObject _overlay;
	private GameObject _unit;
	private GameObject _terrain;

	// Use this for initialization
	void Start () 
	{

	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public GameObject overlay
    {
        get
        {
            return _overlay;
        }
        set
        {
            if (value.GetComponent<Overlay>() != null)
            {
                clearOverlay();
				_overlay = value;
                _overlay.transform.position = new Vector3(transform.position.x, transform.position.y, 0);
                _overlay.transform.parent = GetComponent<Transform>();
            }
            else {
                Debug.Log("Attempted to assign a non-overlay object into the overlay slot.");
            }
        }
    }

    public GameObject unit
    {
        get
        {
            return _unit;
        }
        set
        {
            if (value.GetComponent<Unit>() != null)
            {
                clearUnit();
				_unit = value;
                _unit.transform.position = new Vector3(transform.position.x, transform.position.y, 0);
                _unit.transform.parent = GetComponent<Transform>();
            }
            else {
                Debug.Log("Attempted to assign a non-unit object into the unit slot.");
            }
        }
    }

    public GameObject terrain
    {
        get
        {
            return _terrain;
        }
        set
        {
            if (value.GetComponent<MapTerrain>() != null)
            {
                clearTerrain();
				_terrain = value;
                _terrain.transform.position = new Vector3(transform.position.x, transform.position.y, 0);
                _terrain.transform.parent = GetComponent<Transform>();
            }
            else {
                Debug.Log("Attempted to assign a non-terrain object into the terrain slot.");
            }
        }
    }

	public void clearOverlay()
	{
		Destroy (_overlay);
        _overlay = null;
	}

	public void clearUnit()
	{
		Destroy (_unit);
        _unit = null;
	}

	public void clearTerrain()
	{
		Destroy (_terrain);
        _terrain = null;
	}


}
