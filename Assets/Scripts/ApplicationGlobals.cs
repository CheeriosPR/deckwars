using UnityEngine;

public class ApplicationGlobals
{
	private static InputManager input = null;
    public static readonly Vector2 NULL_VECTOR = new Vector2(int.MaxValue, int.MaxValue);

    public static InputManager getInputManager()
    {
        if (input == null)
        {
            input = new InputManager();
        }
	    return input;
	}
}