﻿using UnityEngine;
using System.Collections.Generic;

public class MapHandler : MonoBehaviour {

	private GameObject[,] tiles;
	public GameObject cursor;
	public int mapWidth;
	public int mapHeight;

	private PrefabLibrary library;

    //used to hold selected data so it doesn't reset each frame
    private Vector2 selectedTilePos = ApplicationGlobals.NULL_VECTOR;
    private Vector2 prevCursorTilePos = ApplicationGlobals.NULL_VECTOR;
    private List<Vector2> tileSpread = null;
    private List<GameObject> pathSelectedToCursor = null;

	// Use this for initialization
	void Start () {
		tiles = new GameObject[mapWidth,mapHeight]; //initialize array of GameObjects based on the width and the map of the height
		library = GetComponent<PrefabLibrary>();
		//instantiate our empty tiles
		for (int i = 0; i < tiles.GetLength (0); ++i) {
			for (int j = 0; j < tiles.GetLength (1); ++j) {
				tiles [i, j] = Instantiate (library.tile);
				tiles [i, j].transform.Translate (i, j, 0);
				tiles [i, j].transform.parent = GetComponent<Transform> ();
				tiles [i ,j].GetComponent<Tile>().terrain = Instantiate(library.terrain[MapTerrain.TerrainType.PLAINS]);
            }
		}

		//cursor is attached in Unity, just send it the map size
		cursor.GetComponent<PlayerController> ().xBound = mapWidth - 1;
		cursor.GetComponent<PlayerController> ().yBound = mapHeight - 1;

		//Create the map using the prefabs placed in unity which are children of the map
		for(int i = transform.childCount - 1; i >= 0; i--) {
            Transform child = transform.GetChild(i);
            //if a terrain piece, do following
            if (child.gameObject.GetComponent<MapTerrain>() != null) {
                tiles [(int)child.position.x, (int)child.position.y].GetComponent<Tile> ().terrain = child.gameObject;
            }
			//if a unit, do following
			else if (child.gameObject.GetComponent<Unit>() != null) {
                tiles [(int)child.position.x, (int)child.position.y].GetComponent<Tile> ().unit = child.gameObject;
            }
        }
    }

	// Update is called once per frame
	void Update () {
        //get if z was pressed and the object and tile at the cursor's location
        bool zPressed = ApplicationGlobals.getInputManager().getInput("Select");
        Vector2 cursorTilePos = new Vector2((int)cursor.transform.position.x, (int)cursor.transform.position.y);
        GameObject objAtCursor = tiles[(int) cursorTilePos.x, (int) cursorTilePos.y];
        Tile tileAtCursor = objAtCursor.GetComponent<Tile>();

        //if we've selected a tile to move and the cursor has moved in the last frame, calculate the path to the cursor
        if (selectedTilePos != ApplicationGlobals.NULL_VECTOR && cursorTilePos != prevCursorTilePos)
        {
            if (pathSelectedToCursor != null)
            {
                foreach (GameObject g in pathSelectedToCursor)
                {
                    Destroy(g);
                }
                pathSelectedToCursor = null;
            }
            pathSelectedToCursor = getPathObjects(getPathFromSpread(selectedTilePos, cursorTilePos));
        }
        else if (selectedTilePos != ApplicationGlobals.NULL_VECTOR && cursorTilePos == selectedTilePos)
        {
            if (pathSelectedToCursor != null)
            {
                foreach (GameObject g in pathSelectedToCursor)
                {
                    Destroy(g);
                }
                pathSelectedToCursor = null;
            }
        }

        //update the previous cursor position
        prevCursorTilePos = cursorTilePos;

        //if z was pressed...
        if (zPressed)
        {
            //a unit has not yet been selected and the tile the cursor is over has a unit
            if (selectedTilePos == ApplicationGlobals.NULL_VECTOR && tileAtCursor.unit != null)
            {
                //set this tile as our selectedTile
                selectedTilePos = cursorTilePos;

                //apply overlay by using tileSpread. it is controlled by the getMoveableTiles method call and shouldn't be messed with
                getMoveableTiles(tileAtCursor.unit.GetComponent<Unit>(),
                    (int)tileAtCursor.transform.position.x,
                    (int)tileAtCursor.transform.position.y);
                foreach (Vector2 square in tileSpread)
                {
                    Tile thisTile = tiles[(int)square.x, (int)square.y].GetComponent<Tile>();
                    if (thisTile.unit != null)
                    {
                        thisTile.overlay = Instantiate(library.overlay[Overlay.OverlayType.MOVE_ERROR]);
                    }
                    else
                    {
                        thisTile.overlay = Instantiate(library.overlay[Overlay.OverlayType.MOVE]);
                    }
                }
                cursor.GetComponent<PlayerController>().limit = tileSpread; //limit the cursor to only move within the spread
                tileAtCursor.overlay = Instantiate(library.overlay[Overlay.OverlayType.SELECTED]);
            }
            //a unit is already selected (the spread is drawn and stuff) and the tile the cursor is over doesn't have a unit
            else if (selectedTilePos != ApplicationGlobals.NULL_VECTOR && tileAtCursor.unit == null)
            {
                GameObject objAtSelection = tiles[(int)selectedTilePos.x, (int)selectedTilePos.y];
                Tile tileAtSelection = objAtSelection.GetComponent<Tile>();
                //move by cloning the unit and clear the old tile
                tileAtCursor.unit = Instantiate(tileAtSelection.unit);
                tileAtSelection.clearUnit();

                //remove the overlay and clear the path variable
                foreach (Vector2 square in tileSpread)
                {
                    tiles[(int)square.x, (int)square.y].GetComponent<Tile>().clearOverlay();
                }
                selectedTilePos = ApplicationGlobals.NULL_VECTOR;
                tileSpread = null;
                foreach(GameObject g in pathSelectedToCursor)
                {
                    Destroy(g);
                }
                pathSelectedToCursor = null;
                cursor.GetComponent<PlayerController>().limit = null; //unbind the cursor
            }
            //the cursor hasn't moved, just drop the overlay and unit and don't do anything else with it
            else if (cursorTilePos == selectedTilePos)
            {
                //remove the overlay and clear the path variable
                foreach (Vector2 square in tileSpread)
                {
                    tiles[(int)square.x, (int)square.y].GetComponent<Tile>().clearOverlay();
                }
                selectedTilePos = ApplicationGlobals.NULL_VECTOR;
                tileSpread = null;
                foreach (GameObject g in pathSelectedToCursor)
                {
                    Destroy(g);
                }
                pathSelectedToCursor = null;
                cursor.GetComponent<PlayerController>().limit = null; //unbind the cursor
            }
        }
	}

	//this is a method that returns an array of points (vector2s) around an origin point based on the unit's range and if it can pass through terrain with movement penalty
	private void getMoveableTiles(Unit unit, int originX, int originY, bool excludeCenter = false)
	{
        //tileSpread is made and populated with the origin if needed
        tileSpread = new List<Vector2>();
        if (!excludeCenter)
        {
            tileSpread.Add(new Vector2(originX, originY));
        }
        MapTerrain terrain = tiles[originX, originY].GetComponent<Tile>().terrain.GetComponent<MapTerrain>();
        //call the recursive function to get ALL of the spots we can move to
        List<Vector2> tempPoints1 = getMoveableHelper(unit, unit.range - terrain.movPenalty, originX + 1, originY);
        List<Vector2> tempPoints2 = getMoveableHelper(unit, unit.range - terrain.movPenalty, originX - 1, originY);
        List<Vector2> tempPoints3 = getMoveableHelper(unit, unit.range - terrain.movPenalty, originX, originY + 1);
        List<Vector2> tempPoints4 = getMoveableHelper(unit, unit.range - terrain.movPenalty, originX, originY - 1);
        //add all the calculated points to the tilespread
        tileSpread.AddRange(tempPoints1);
        tileSpread.AddRange(tempPoints2);
        tileSpread.AddRange(tempPoints3);
        tileSpread.AddRange(tempPoints4);

        //remove duplicate entries if there are any
        HashSet<Vector2> dupeRemover = new HashSet<Vector2>(tileSpread);
        tileSpread = new List<Vector2>(dupeRemover);
    }

    private List<Vector2> getMoveableHelper(Unit unit, int rangeLeft, int startX, int startY)
    {
        List<Vector2> finalPoints = new List<Vector2>();
        //stop here if out of bounds or there's no range left 
        if (startX < 0 || 
            startX >= mapWidth || 
            startY < 0 || 
            startY >= mapHeight || 
            rangeLeft <= 0)
        {
            return finalPoints;
        }

        //not out of bounds, grab the current tile and terrain for checking impassibility
        Tile currentTile = tiles[startX, startY].GetComponent<Tile>();
        MapTerrain currentTerrain = currentTile.terrain.GetComponent<MapTerrain>();
        if (unit.isVehicle() && !currentTerrain.isDrivable)
        {
            return finalPoints;
        }

        //do recursive calls on all the adjacent squares, knocking off the terrain movement penalty
        List<Vector2> tempPoints1 = getMoveableHelper(unit, rangeLeft - currentTerrain.movPenalty, startX + 1, startY);
        List<Vector2> tempPoints2 = getMoveableHelper(unit, rangeLeft - currentTerrain.movPenalty, startX - 1, startY);
        List<Vector2> tempPoints3 = getMoveableHelper(unit, rangeLeft - currentTerrain.movPenalty, startX, startY + 1);
        List<Vector2> tempPoints4 = getMoveableHelper(unit, rangeLeft - currentTerrain.movPenalty, startX, startY - 1);

        //add the points from recursive calls to the list and return it
        finalPoints.AddRange(tempPoints1);
        finalPoints.AddRange(tempPoints2);
        finalPoints.AddRange(tempPoints3);
        finalPoints.AddRange(tempPoints4);

        //we can go here, but maybe not be able to drop the unit here. we can check that before we drop the unit in the main update function
        finalPoints.Add(new Vector2(startX, startY));

        return finalPoints;
    }

    //an implementation of Dijkstra's algorithm for path finding
    public List<Vector2> getPathFromSpread(Vector2 start, Vector2 end)
    {
        //if there's no spread to calculate off of, then we can't return any path
        if(tileSpread == null)
        {
            return null;
        }

        //set up all of the distances to compare against, where the distance always includes a heuristic cost to the end node (manhattan distance)
        Dictionary<Vector2, int> squareCosts = new Dictionary<Vector2, int>();
        foreach (Vector2 square in tileSpread)
        {
            squareCosts.Add(square, int.MaxValue);
        }
        squareCosts[start] = (int)(Mathf.Abs(end.x - start.x) + Mathf.Abs(end.y - start.y));

        //set up a previous node dictionary for later path reconstruction
        Dictionary<Vector2, Vector2> nodePrevNode = new Dictionary<Vector2, Vector2>();
        foreach(Vector2 square in tileSpread)
        {
            nodePrevNode.Add(square, ApplicationGlobals.NULL_VECTOR);
        }
        nodePrevNode[start] = start;

        //set up the unvisited set and set up the current square we're at (where the unit currently is)
        List<Vector2> unvisited = new List<Vector2>(tileSpread);
        Vector2 current = start;

        //while we haven't visited every node in the unvisited set
        while (unvisited.Count != 0)
        {
            //mark the current node as visited
            unvisited.Remove(current);

            //grab all the neighbors from the current node
            List<Vector2> neighborTemp = new List<Vector2>();
            List<Vector2> neighbors = new List<Vector2>();
            neighbors.Add(new Vector2(current.x - 1, current.y));
            neighbors.Add(new Vector2(current.x + 1, current.y));
            neighbors.Add(new Vector2(current.x, current.y - 1));
            neighbors.Add(new Vector2(current.x, current.y + 1));
            //prune off neighbors that aren't in the tilespread
            foreach(Vector2 neighbor in neighbors)
            {
                if(tileSpread.Contains(neighbor))
                {
                    neighborTemp.Add(neighbor);
                }
            }
            neighbors = neighborTemp;

            //for each neighbor that hasn't been visited yet calculate a tentative value and store it in the dictionary if it is smaller
            foreach(Vector2 neighbor in neighbors)
            {
                if(unvisited.Contains(neighbor))
                {
                    int valueOfCurrentNode, currentValueOfNeighbor, calculatedValueOfNeighbor;
                    squareCosts.TryGetValue(current, out valueOfCurrentNode);
                    squareCosts.TryGetValue(neighbor, out currentValueOfNeighbor);
                    //calculated value is the current value plus movement penalty for this square plus the manhattan distance to the end square
                    calculatedValueOfNeighbor = valueOfCurrentNode 
                        + tiles[(int)current.x, (int)current.y].GetComponent<Tile>().terrain.GetComponent<MapTerrain>().movPenalty
                        + (int) (Mathf.Abs(end.x - neighbor.x) + Mathf.Abs(end.y - neighbor.y));
                    if(calculatedValueOfNeighbor < currentValueOfNeighbor)
                    {
                        squareCosts[neighbor] = calculatedValueOfNeighbor;
                        nodePrevNode[neighbor] = current;
                    }
                }
            }

            //if the destination isn't in unvisited (i.e. has been visited) we can exit the loop to calculate the final path
            if(!unvisited.Contains(end))
            {
                break;
            }

            //calculate the smallest distance in unvisited
            int smallest = int.MaxValue;
            Vector2 smallestNode = ApplicationGlobals.NULL_VECTOR;
            foreach(Vector2 square in unvisited)
            {
                int value;
                squareCosts.TryGetValue(square, out value);
                if (value <= smallest)
                {
                    smallest = value;
                    smallestNode = square;
                }
            }

            //if the smallest value is still int.MaxValue, we can stop because there's no connection to some nodes (should never happen)
            if(smallest == int.MaxValue)
            {
                break;
            }

            //otherwise set the current node to the smallest value node in unvisited and continue the algorithm
            current = smallestNode;
        }

        //time to reconstruct the final path, set up a list to return and construct the path from the end to the start
        List<Vector2> path = new List<Vector2>();
        path.Add(end);

        Vector2 previousNode;
        nodePrevNode.TryGetValue(end, out previousNode);
        while (previousNode != start)
        {
            path.Add(previousNode);
            nodePrevNode.TryGetValue(previousNode, out previousNode);
        }

        path.Add(start);
        path.Reverse();
        return path;
    }

    public List<GameObject> getPathObjects(List<Vector2> pathList)
    {
        //there's no path to draw if there's less than 2 path bits
        if (pathList.Count < 2) return null;
        List<GameObject> pathSprites = new List<GameObject>(pathList.Count);

        //set up tracking for path calculation
        Vector2 prev = ApplicationGlobals.NULL_VECTOR;
        Vector2 cur = pathList[0];
        Vector2 next = pathList[1];

        //add the starting node on it's own by calculating deltas and picking the starting tile accordingly
        float dx = next.x - cur.x;
        float dy = next.y - cur.y;
        int startIndex = dx > 0 ? 2 : (dx == 0 ? (dy > 0 ? 0 : 1) : 3);
        GameObject pathTile = Instantiate(library.pathPrefabs[startIndex]);
        pathTile.transform.position = new Vector3(cur.x, cur.y, 2);
        pathSprites.Add(pathTile);

        //for every node except the end one, pick the right tile piece
        for(int i = 1; i < pathList.Count - 1; i++)
        {
            //increment the tracking first
            prev = cur;
            cur = next;
            next = pathList[i + 1];

            //calculate deltas
            float dx_p = cur.x - prev.x;
            float dy_p = cur.y - prev.y;
            float dx_n = next.x - cur.x;
            float dy_n = next.y - cur.y;
            
            //pick the directions based on those deltas for prev -> cur and cur -> next
            /* east is 1, west 3, north 2, south 4
            *      ---
            *      |2|
            *   ---------
            *   |3| c |1|
            *   ---------
            *      |4|
            *      ---
            */
            int dir_p = dx_p > 0 ? 1 : (dx_p == 0 ? (dy_p > 0 ? 2 : 4) : 3);
            int dir_n = dx_n > 0 ? 1 : (dx_n == 0 ? (dy_n > 0 ? 2 : 4) : 3);
            //get the index based on those deltas
            /*   1     2     3     4      dir_n
            * 1  9     11    -     13
            * 2  12    8     13    -
            * 3  -     10    9     12
            * 4  10    -     11    8
            *
            * dir_p
            */
            int index = dir_p == 1 ? (dir_n == 1 ? 9 : (dir_n == 2 ? 11 : 13)) :
                        dir_p == 2 ? (dir_n == 1 ? 12 : (dir_n == 2 ? 8 : 13)) :
                        dir_p == 3 ? (dir_n == 3 ? 9 : (dir_n == 4 ? 12 : 10)) : 
                        dir_n == 3 ? 11 : (dir_n == 4 ? 8 : 10);

            pathTile = Instantiate(library.pathPrefabs[index]);
            pathTile.transform.position = new Vector3(cur.x, cur.y, 2);
            pathSprites.Add(pathTile);
        }

        dx = next.x - cur.x;
        dy = next.y - cur.y;
        int endIndex = dx > 0 ? 6 : (dx == 0 ? (dy > 0 ? 4 : 5) : 7);
        pathTile = Instantiate(library.pathPrefabs[endIndex]);
        pathTile.transform.position = new Vector3(next.x, next.y, 2);
        pathSprites.Add(pathTile);

        return pathSprites;
    }
}