﻿using UnityEngine;
using System.Collections.Generic;

public class PrefabLibrary : MonoBehaviour {
	[System.Serializable]
	public struct UnitPrefab {
		public Unit.UnitType type;
		public GameObject prefab;
	};

	public UnitPrefab[] unitPrefabs;
	public Dictionary<Unit.UnitType, GameObject> unit;

	[System.Serializable]
	public struct TerrainPrefab {
		public MapTerrain.TerrainType type;
		public GameObject prefab;
	};

	public TerrainPrefab[] terrainPrefabs;
	public Dictionary<MapTerrain.TerrainType, GameObject> terrain;

	[System.Serializable]
	public struct OverlayPrefab {
		public Overlay.OverlayType type;
		public GameObject prefab;
	};

	public OverlayPrefab[] overlayPrefabs;
	public Dictionary<Overlay.OverlayType, GameObject> overlay;

    public GameObject[] pathPrefabs;

    public GameObject tile;

	// Use this for initialization
	void Start () {
		unit = new Dictionary<Unit.UnitType, GameObject> ();
		terrain = new Dictionary<MapTerrain.TerrainType, GameObject> ();
		overlay = new Dictionary<Overlay.OverlayType, GameObject> ();

		for (int i = 0; i < unitPrefabs.Length; ++i) {
			if(!unit.ContainsKey(unitPrefabs[i].type)) {
				unit.Add(unitPrefabs[i].type, unitPrefabs[i].prefab);
			}
		}
		for (int i = 0; i < terrainPrefabs.Length; ++i) {
			if(!terrain.ContainsKey(terrainPrefabs[i].type)) {
				terrain.Add(terrainPrefabs[i].type, terrainPrefabs[i].prefab);
			}
		}
		for (int i = 0; i < overlayPrefabs.Length; ++i) {
			if(!overlay.ContainsKey(overlayPrefabs[i].type)) {
				overlay.Add(overlayPrefabs[i].type, overlayPrefabs[i].prefab);
			}
		}
	}
}
