﻿using UnityEngine;
using System.Collections;

public class MapTerrain : MonoBehaviour {
    public enum TerrainType
    {
        PLAINS,
        MOUNTAIN,
        RIVER
    };
    public TerrainType type;
    public int defStars = 0;
	public int movPenalty = 0;
    public bool isDrivable = false;
	public bool isCapturable = false;

	// Use this for initialization
    // Depending on the TerrainType of the tile, we need to assign it's properties
	void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
