﻿using UnityEngine;
using System.Collections;

public class CameraFollower : MonoBehaviour
{

    public GameObject toFollow;       //Public variable to store a reference to the game object to follow
    private Vector3 offset;         //Private variable to store the offset distance between the followed object and camera

    // Use this for initialization
    void Start()
    {
        //Calculate and store the offset value by getting the distance between the object's position and camera's position.
        offset = transform.position - toFollow.transform.position;
    }

    // LateUpdate is called after Update each frame
    void LateUpdate()
    {
        // Set the position of the camera's transform to be the same as the object's, but offset by the calculated offset distance.
        transform.position = toFollow.transform.position + offset;
    }

    //for setting the camera's follow target. the game engine should control this
    public void changeFollowTarget(GameObject obj)
    {
        toFollow = obj;
        offset = transform.position - toFollow.transform.position;
    }
}