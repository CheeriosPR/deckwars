﻿using UnityEngine;
using System.Collections.Generic;
public class PlayerController : MonoBehaviour {

	//reference to the transform component
	private Transform trans;

	//event flags and variables for tracking the state of the cursor limit (if it's bounded by more than just the map)
	private bool canMove;
	private bool _locked;
	private List<Vector2> _limit;

	//properties of the cursor
	//x and y bounds are set by the map that it inhabits, according to its dimensions
	public float speed = 1.0f;
	public int xBound;
	public int yBound;

	// Use this for initialization
	void Start () 
	{
		//initialize reference to the transform component
		trans = GetComponent<Transform> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
        if(_locked)
        {
            return;
        }
        //local flags for which keys are down
        bool moveLeft = ApplicationGlobals.getInputManager().getInput("Left");
        bool moveRight = ApplicationGlobals.getInputManager().getInput("Right");
        bool moveUp = ApplicationGlobals.getInputManager().getInput("Up");
        bool moveDown = ApplicationGlobals.getInputManager().getInput("Down");

        //i wanted to limit it to not scroll if you just held the key down (though this system is bound to change after a round of polish)
        //so you can't move until you're not holding any key down for a frame.
        if (!moveRight && !moveLeft && !moveUp && !moveDown) {
			canMove = true;
		} 

		//these if statements are basically just a logic tree that moves the cursor up, down, left or right depending on if it's a valid move.
		else if (canMove && !_locked && _limit == null) {
			if (moveRight && !(trans.position.x + 1 > xBound)) {
				trans.Translate (speed, 0, 0);
			} else if (moveLeft && trans.position.x != 0) {
				trans.Translate (speed * -1, 0, 0);
			} else if (moveUp && !(trans.position.y + 1 > yBound)) {
				trans.Translate (0, speed, 0);
			} else if (moveDown && trans.position.y != 0) {
				trans.Translate (0, speed * -1, 0);
			}
            canMove = false;
		}
		//these if statements are moving the cursor if it's limited to some area for whatever reason.
		//these are seperate because i didn't want to have to check them and make the computer do math every time if there wasn't even a limit applied.

		else if (!_locked && _limit != null) {
			if (moveRight && !(trans.position.x + 1 > xBound) && _limit.Contains(new Vector2(trans.position.x + 1, trans.position.y))) {
				trans.Translate (speed, 0, 0);
			} else if (moveLeft && trans.position.x != 0 && _limit.Contains(new Vector2(trans.position.x - 1, trans.position.y))) {
				trans.Translate (speed * -1, 0, 0);
			} else if (moveUp && !(trans.position.y + 1 > yBound) && _limit.Contains(new Vector2(trans.position.x, trans.position.y + 1))) {
				trans.Translate (0, speed, 0);
			} else if (moveDown && trans.position.y != 0 && _limit.Contains(new Vector2(trans.position.x, trans.position.y - 1))) {
				trans.Translate (0, speed * -1, 0);
			}
            canMove = false;
		}
	}

    public bool lockController
    {
        get
        {
            return _locked;
        }
        set
        {
            _locked = value;
        }
    }

    public List<Vector2> limit
    {
        //set this to a list of points that the cursor can move to
        get
        {
            return _limit;
        }

        //setting this null turns off the limit
        set
        {
            _limit = value;
        }
    }
}
