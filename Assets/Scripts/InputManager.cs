﻿using UnityEngine;
using System.Collections.Generic;

public class InputManager {
    //stores the key mappings
    private Dictionary<string, KeyCode> keyMap;
    //stores the data scraped from each key
    private Dictionary<string, bool> inputMap;

	public InputManager () {
        keyMap = new Dictionary<string, KeyCode>();
        inputMap = new Dictionary<string, bool>();

        //Set up some defaults here
        keyMap.Add("Left", KeyCode.LeftArrow);
        keyMap.Add("Right", KeyCode.RightArrow);
        keyMap.Add("Up", KeyCode.UpArrow);
        keyMap.Add("Down", KeyCode.DownArrow);
        keyMap.Add("Select", KeyCode.Z);

        //scrape inputs one time to get everything set up
        scrapeInputs();
    }
	
    //collects input data
	public void scrapeInputs () {
        foreach (string s in keyMap.Keys) {
            KeyCode k;
            keyMap.TryGetValue(s, out k);
            if (!inputMap.ContainsKey(s))
            {
                inputMap.Add(s, Input.GetKeyDown(k));
            }
            else
            {
                inputMap[s] = Input.GetKeyDown(k);
            }
        }
    }

    //changes the key mapping of the plaintext key name to the associated new keycode
    public bool changeKeyMap(string inputName, KeyCode newKey)
    {
        if(!keyMap.ContainsKey(inputName))
        {
            keyMap.Add(inputName, newKey);
            return true;
        }
        else
        {
            keyMap[inputName] = newKey;
        }
        return false;
    }

    //allows for grabbing single inputs from the manager if needed
    //note that if the input name doesn't exist in the keymap then the return value will always be false
    public bool getInput(string inputName)
    {
        bool output;
        inputMap.TryGetValue(inputName, out output);
        return output;
    }
}
