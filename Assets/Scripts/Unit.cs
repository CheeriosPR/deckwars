﻿using UnityEngine;
using System.Collections;

public class Unit : MonoBehaviour {

    public enum UnitType
    {
        INFANTRY, 
        TANK
    };
    public int range;
    public UnitType type;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public bool isVehicle()
    {
        switch(type)
        {
            case UnitType.TANK:
                return true;
        }
        return false;
    }

    public bool isBiped()
    {
        switch (type)
        {
            case UnitType.INFANTRY:
                return true;
        }
        return false;
    }
}
